﻿using maisfidelidade.cnc.bo.api.core.Domain.Dashboard;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain.Extrato
{
    public class ExtratoCompleto
    {
        public DateTime Mes { get; set; }
        public string MesExtenso { get; set; }
        public IList<DashboardCardItem> Cards { get; set; }
        public IList<DashboardUltimosExtratos> Semanais { get; set; }
        public DashboardUltimosExtratos Mensal { get; set; }
    }
}
