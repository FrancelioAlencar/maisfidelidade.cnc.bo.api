﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain
{
    public class BeneficioResgatado
    {
        public string CNPJ { get; set; }
        public string Categoria { get; set; }
        public string Beneficio { get; set; }
        public int Porcentagem { get; set; }         
        public DateTime DataInclusao { get; set; }
        public bool AcaoSejaPlatinum { get; set; }
    }
}
