﻿using maisfidelidade.cnc.bo.api.core.Domain.Enums;
using maisfidelidade.cnc.bo.api.core.Infrastructure;
using maisfidelidade.cnc.bo.api.core.Common.Log;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain
{
    public interface ILogMongoRepositorio : IMongoRepositorio<LogInfo>
    {
        ObjectId? Salvar(LogInfo entidade);
        IList<LogInfo> Obter(ObjectId idArquivo);
    }
}
