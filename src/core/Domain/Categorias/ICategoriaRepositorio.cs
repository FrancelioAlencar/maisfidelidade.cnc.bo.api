﻿using maisfidelidade.cnc.bo.api.core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain
{
    public interface ICategoriaRepositorio : IRepositorio<Categoria>
    {
        IList<BeneficioCategoria> BuscarBeneficiosCategoriaCustomizadosPor(int idCliente);
    }
}
