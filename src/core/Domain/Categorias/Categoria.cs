﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain
{
    /// <summary>
    /// Categoria
    /// </summary>
    public class Categoria
    {
        public Categoria()
        {
            Beneficios = new List<BeneficioCategoria>();
        }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Nome { get; set; }

        [JsonProperty("benefitList")]
        public IList<BeneficioCategoria> Beneficios { get; set; }
    }
}
