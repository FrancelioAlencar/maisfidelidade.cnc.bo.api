﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain
{
    public class RelatorioResgate
    {
        public RelatorioResgate()
        {
            HistorioMensal = new List<RelatorioResgateItem>();
        }

        [JsonProperty("current_month")]
        public RelatorioResgateItem MesAtual { get; set; }

        [JsonProperty("monthly_history")]
        public IList<RelatorioResgateItem> HistorioMensal { get; set; }
    }
}
