﻿using maisfidelidade.cnc.bo.api.core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain
{
    public interface IResgateRepositorio : IRepositorio<RelatorioResgate>
    {
        IList<RelatorioResgateItem> ObterUltimosResgates();
        IList<BeneficioResgatado> ObterResgate(int IdArquivo, DateTime dataInicio, DateTime dataFim);
    }
}
