﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain.Enums
{
    public enum TipoArquivoEnum
    {
        Semanal = 1,
        Mensal = 2
    }
}
