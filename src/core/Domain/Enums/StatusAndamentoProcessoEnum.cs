﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain.Enums
{
    public enum StatusAndamentoProcessoEnum
    {
        UploadDoArquivo = 0,
        EfetuandoValidacaoFisica = 1,
        EfetuandoValidacaoLogica = 2,
        ProcessandoComplementosWebmotors = 3,
        ProcessandoApuracao = 4,
        AguardandoPublicacao = 5,
        PublicandoImportacao = 6,
        Concluido = 7,
        Cancelado = 8
    }
}
