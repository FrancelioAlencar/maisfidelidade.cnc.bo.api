﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain
{
    public class CriterioCliente
    {
        [JsonProperty("id")]
        public string ID { get; set; }

        [JsonProperty("id_criterio")]
        public string IdCriterio { get; set; }

        [JsonProperty("nome")]
        public string Nome { get; set; }

        [JsonProperty("descricao")]
        public string Descricao { get; set; }

        [JsonProperty("valor")]
        public int Valor { get; set; }

        [JsonProperty("id_empresa")]
        public int IdEmpresa { get; set; }

        [JsonProperty("id_tipo_criterio")]
        public int IdTipoCriterio { get; set; }

        [JsonProperty("pontuacao_atual")]
        public int PontuacaoAtual { get; set; }

        [JsonProperty("porcentagem_atual")]
        public int PorcentagemAtual { get; set; }

        [JsonProperty("pontuacao_necessaria")]
        public int PontuacaoNecessaria { get; set; }

        [JsonProperty("porcentagem_necessaria")]
        public int PorcentagemNecessaria { get; set; }
    }
}
