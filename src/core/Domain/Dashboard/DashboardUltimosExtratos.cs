﻿using maisfidelidade.cnc.bo.api.core.Domain.Importacao;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain.Dashboard
{
    public class DashboardUltimosExtratos
    {
        public string IdArquivo { get; set; }
        public string NomeArquivo { get; set; }
        public TipoArquivoEnum TipoArquivo { get; set; }
        public DateTime DataReferencia { get; set; }
        public string Data { get; set; }
        public string LinkArquivo { get; set; }
        public string LinkExtrato { get; set; }
        public int QuantidadeLinhas { get; set; }
        public bool Publicado { get; set; }
    }
}
