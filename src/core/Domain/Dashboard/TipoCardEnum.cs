﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain.Dashboard
{
	public enum TipoCardEnum : int
	{
		RegistrosProcessados = 1,
		RegistrosComErros = 2,
		ArquivosProcessados = 3
	}
}
