﻿using maisfidelidade.cnc.bo.api.core.Domain.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain
{
    [BsonIgnoreExtraElements]
    public class StatusProcessamentoArquivo
    {
        [BsonId]
        [JsonProperty("file_id")]
        public ObjectId IdArquivo { get; set; }

        [BsonElement("dt_importacao")]
        public DateTime DataImportacao { get; set; }

        [BsonElement("nome")]
        public string Nome { get; set; }

        [BsonElement("link_arquivo")]
        [JsonProperty("link_arquivo")]
        public string LinkArquivo { get; set; }

        [BsonElement("link_extrato")]
        [JsonProperty("link_extrato")]
        public string LinkExtrato { get; set; }

        [BsonElement("id_status_andamento_processo")]
        [JsonProperty("step")]
        public int Etapa { get; set; }

        [BsonElement("id_status_arquivo")]
        [JsonIgnore]
        public int Status { get; set; }

        [BsonElement("tipo_arquivo")]
        public TipoArquivoEnum? TipoArquivo { get; set; }

        [JsonProperty("error")]
        public bool Erro
        {
            get => (Status == 5 || Status == 8);
        }

        [JsonProperty("messages")]
        public IList<string> Mensagem { get; set; }
    }
}
