﻿using MongoDB.Bson;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain
{
    public class StatusUploadArquivo
    {
        [JsonProperty("file_id")]
        public ObjectId? IdArquivo { get; set; }

        [JsonProperty("error")]
        public bool Erro { get; set; }

        [JsonProperty("message")]
        public string Mensagem { get; set; }
    }
}
