﻿using maisfidelidade.cnc.bo.api.core.Domain.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain
{
    public class Arquivo
    {
        public Arquivo(string nome, DateTime dataImportacao, DateTime? dataReferencia, DateTime? dataPrevisaoAtualizacao, TipoArquivoEnum? tipoArquivo, DateTime? dataOrigem, StatusAndamentoProcessoEnum statusAndamentoProcesso, int qtdLinhasProcessadas, int idUsuario)
        {
            Nome = nome;
            DataImportacao = dataImportacao;
            DataReferencia = dataReferencia;
            DataPrevisaoAtualizacao = dataPrevisaoAtualizacao;
            DataOrigem = dataOrigem;
            TipoArquivo = tipoArquivo;
            StatusArquivo = StatusArquivoEnum.Pendente;
            StatusAndamentoProcesso = statusAndamentoProcesso;
            CriteriosProcessado = false;
            CampanhasProcessada = false;
            IndicadoresProcessado = false;
            ExtratoProcessado = false;
            QtdLinhasProcessadas = qtdLinhasProcessadas;
            IdUsuario = idUsuario;
        }

        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("nome")]
        public string Nome { get; set; }

        [BsonElement("dt_importacao")]
        public DateTime DataImportacao { get; set; }

        [BsonElement("dt_referencia")]
        public DateTime? DataReferencia { get; set; }

        [BsonElement("dt_previsao_atualizacao")]
        public DateTime? DataPrevisaoAtualizacao { get; set; }

        [BsonElement("dt_origem")]
        public DateTime? DataOrigem { get; set; }

        [BsonElement("tipo_arquivo")]
        public TipoArquivoEnum? TipoArquivo { get; set; }

        [BsonElement("id_status_arquivo")]
        public StatusArquivoEnum StatusArquivo { get; set; }

        [BsonElement("id_status_andamento_processo")]
        public StatusAndamentoProcessoEnum StatusAndamentoProcesso { get; set; }

        [BsonElement("criterios_processado")]
        public bool CriteriosProcessado { get; set; }

        [BsonElement("campanhas_processada")]
        public bool CampanhasProcessada { get; set; }

        [BsonElement("indicadores_processado")]
        public bool IndicadoresProcessado { get; set; }

        [BsonElement("extrato_processado")]
        public bool ExtratoProcessado { get; set; }

        [BsonElement("acao_seja_platinum_processada")]
        public bool AcaoSejaPlatinumProcessada { get; set; }

        [BsonElement("qtd_linhas")]
        public int QtdLinhasProcessadas { get; set; }

        [BsonElement("id_usuario")]
        public int IdUsuario { get; set; }

        [BsonElement("link_arquivo")]
        public string LinkArquivo { get; set; }

        [BsonElement("link_extrato")]
        public string LinkExtrato { get; set; }
    }
}
