﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain.Importacao
{
    public enum StatusAndamentoProcessoEnum
    {
        EfetuandoValidacaoFisica = 1,
        EfetuandoValidacaoLogica = 2,
        ProcessandoComplementosWebmotors = 3,
        ProcessandoCriterios = 4,
        ProcessandoCampanhas = 5,
        ProcessandoIndicadores = 6,
        ProcessandoExtrato = 7,
        ProcessandoAcaoPlatinum = 8,
        PublicandoImportacao = 9,
        Concluido = 10
    }
}
