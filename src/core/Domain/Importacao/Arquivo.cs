﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain.Importacao
{
    public class Arquivo
    {
        [JsonProperty("id_arquivo")]
        public int id_arquivo { get; set; }

        public string nome { get; set; }
        public DateTime dt_importacao { get; set; }
        public DateTime dt_referencia { get; set; }
        public DateTime dt_previsao_atualizacao { get; set; }
        public DateTime dt_origem { get; set; }
        public int tipo_arquivo { get; set; }
        public int id_status_processamento { get; set; }
        public int id_status_andamento_processo { get; set; }
        public string object_id { get; set; }
        public string link_arquivo { get; set; }
        public string link_extrato { get; set; }
        public int qtd_linhas { get; set; }
    }
}
