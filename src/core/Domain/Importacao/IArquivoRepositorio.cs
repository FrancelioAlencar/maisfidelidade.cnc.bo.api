﻿using maisfidelidade.cnc.bo.api.core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain.Importacao
{
    public interface IArquivoRepositorio : IRepositorio<Arquivo>
    {
        IList<Arquivo> Obter();
        Arquivo Obter(int idArquivo);
    }
}