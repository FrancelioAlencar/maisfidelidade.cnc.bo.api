﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain.Importacao
{
	public enum TipoArquivoEnum : int
	{
		Semanal = 1,
		Mensal = 2
	}
}
