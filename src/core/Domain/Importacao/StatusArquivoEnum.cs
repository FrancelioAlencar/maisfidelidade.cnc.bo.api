﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain.Importacao
{
	public enum StatusArquivoEnum : int
	{
		Pendente = 1,
		Processando = 2,
		ProcessadoComSucesso = 3,
		ErroAoProcessar = 5,
		AguardandoAprovacao = 4,
		Publicando = 6,
		PublicadoComSucesso = 7,
		ErroAoPublicar = 8
	}
}
