﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain
{
    public class RelatorioResgateItem
    {
        [JsonProperty("file_id")]
        public int IdArquivo { get; set; }

        [JsonIgnore]
        public DateTime DataReferencia { get; set; }

        [JsonProperty("month")]
        public int Mes { get; set; }

        [JsonProperty("year")]
        public int Ano { get; set; }

        [JsonProperty("released")]
        public bool Liberado { get; set; }
    }
}
