﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Domain
{
    [BsonIgnoreExtraElements]
    public class StatusArquivoMessagem
    {
        [BsonElement("mensagem")]
        [JsonProperty("message")]
        public string Mensagem { get; set; }
    }
}
