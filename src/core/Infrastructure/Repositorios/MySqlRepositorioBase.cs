﻿using maisfidelidade.cnc.bo.api.core.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Infrastructure.Repositorios
{
    public abstract class MySqlRepositorioBase<TEntityAggregate> : RepositorioBase<TEntityAggregate>
    {
        public delegate void AppendChildData(IList<TEntityAggregate> entityAggregate,
            object childEntityKeyValue);

        private readonly Database readDatabase;
        private readonly Database writeDatabase;
        private Dictionary<string, AppendChildData> childCallbacks;
        private string baseQuery;
        private string baseWhereClause;

        protected MySqlRepositorioBase()
        {
            this.readDatabase = DatabaseFactory.CreateDatabase("read");
            this.writeDatabase = DatabaseFactory.CreateDatabase("write");
            this.childCallbacks = new Dictionary<string, AppendChildData>();
            this.BuildChildCallbacks();
            this.baseQuery = this.GetBaseQuery();
            this.baseWhereClause = this.GetBaseWhereClause();
        }

        #region Propriedades



        #endregion

        protected Dictionary<string, AppendChildData> ChildCallbacks
        {
            get { return this.childCallbacks; }
        }

        protected abstract void BuildChildCallbacks();

        #region Métodos Abstratos

        protected abstract string GetBaseQuery();
        protected abstract string GetBaseWhereClause();

        #endregion

        #region Métodos Públicos

        public override TEntityAggregate BuscarPor(int id)
        {
            StringBuilder builder = this.GetBaseQueryBuilder();
            builder.Append(this.BuildBaseWhereClause(id));

            return this.BuildEntityFromSql(builder.ToString());
        }

        public override IList<TEntityAggregate> BuscarTodos()
        {
            StringBuilder builder = this.GetBaseQueryBuilder();
            builder.Append(";");

            return this.BuildEntitiesFromSql(builder.ToString());
        }



        #endregion

        #region Métodos Protegidos

        protected virtual StringBuilder GetBaseQueryBuilder()
        {
            var queryBuilder = new StringBuilder();
            queryBuilder.Append(this.baseQuery);      
            
            return queryBuilder;
        }

        protected virtual string BuildBaseWhereClause(int id)
        {
            return string.Format(this.baseWhereClause, id);
        }

        protected IList<TEntityAggregate> ExecuteQuery(string query)
        {
            return this.readDatabase.ExecuteQuery<TEntityAggregate>(CommandType.Text, query);
        }

        protected IList<TEntity> ExecuteQuery<TEntity>(string query)
        {
            return this.readDatabase.ExecuteQuery<TEntity>(CommandType.Text, query);
        }

        protected TEntityAggregate ExecuteQueryFirst(string query)
        {
            return this.readDatabase.ExecuteQueryFirst<TEntityAggregate>(CommandType.Text, query);
        }

        protected TEntity ExecuteQueryFirst<TEntity>(string query)
        {
            return this.readDatabase.ExecuteQueryFirst<TEntity>(CommandType.Text, query);
        }

        protected virtual void BuildEntities(IList<TEntityAggregate> entities)
        {
            if (this.childCallbacks != null && this.childCallbacks.Count > 0)
            {
                foreach (string childKeyName in this.childCallbacks.Keys)
                {
                    this.childCallbacks[childKeyName](entities, null);
                }
            }
        }

        protected virtual TEntityAggregate BuildEntityFromSql(string sql)
        {
            //TEntityAggregate entity = default(TEntityAggregate);

            //entity = this.ExecuteQueryFirst(sql);

            //BuildEntity(entity);

            //return entity;

            throw new NotImplementedException();
        }

        protected virtual IList<TEntityAggregate> BuildEntitiesFromSql(string sql)
        {
            var entities = this.ExecuteQuery(sql);

            BuildEntities(entities);

            return entities;
        }

        #endregion
    }
}
