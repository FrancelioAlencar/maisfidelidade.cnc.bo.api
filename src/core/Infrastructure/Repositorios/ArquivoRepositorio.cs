﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using maisfidelidade.cnc.bo.api.core.Domain.Importacao;

namespace maisfidelidade.cnc.bo.api.core.Infrastructure.Repositorios
{
    public class ArquivoRepositorio : MySqlRepositorioBase<Arquivo>, IArquivoRepositorio
    {
        public ArquivoRepositorio()
        {

        }

        public Arquivo Obter(int idArquivo)
        {
            var builder = this.GetBaseQueryBuilder();
            builder.AppendFormat(" WHERE id_arquivo = {0}", idArquivo);

            return this.ExecuteQuery<Arquivo>(builder.ToString()).FirstOrDefault();
        }

        public IList<Arquivo> Obter()
        {
            var builder = this.GetBaseQueryBuilder();
            builder.Append(this.GetBaseWhereClause());

            return this.BuildEntitiesFromSql(builder.ToString());
        }

        protected override void BuildChildCallbacks()
        {

        }

        #region GetBaseQuery    

        protected override string GetBaseQuery()
        {
            return "SELECT id_arquivo, nome, dt_importacao, STR_TO_DATE(dt_referencia, '%Y-%m-%d') AS dt_referencia, dt_previsao_atualizacao, dt_origem, tipo_arquivo, id_status_processamento, id_status_andamento_processo, object_id, link_arquivo, link_extrato, qtd_linhas FROM db_maisfidelidade.autos_tb_arquivo";
        }

        #endregion

        protected override string GetBaseWhereClause()
        {
            return " ORDER BY dt_referencia desc";
        }
    }
}
