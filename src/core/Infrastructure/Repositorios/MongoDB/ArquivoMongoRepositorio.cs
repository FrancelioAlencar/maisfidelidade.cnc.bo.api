﻿using maisfidelidade.cnc.bo.api.core.Domain;
using maisfidelidade.cnc.bo.api.core.Infrastructure.Repositorios.MongoDB.Base;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace maisfidelidade.cnc.bo.api.core.Infrastructure.Repositorios.MongoDB
{
    public class ArquivoMongoRepositorio : MongoRepositorioBase<Arquivo>, IArquivoMongoRepositorio
    {
        public ArquivoMongoRepositorio()
            : base("arquivos")
        {

        }

        public StatusProcessamentoArquivo ObterStatusAndamentoProcesso(int idUsuario)
        {
            var builderFilter = Builders<StatusProcessamentoArquivo>.Filter;

            var filter = builderFilter.Eq("id_usuario", idUsuario);

            var list = this.ExecuteQuery<StatusProcessamentoArquivo>(this.CollectionName, filter);

            var status = list.ToList().OrderByDescending(x => x.DataImportacao).FirstOrDefault();
            
            if (status != null && status.Erro)
            {
                AdicionarMensagens(status);
            }

            return status;
        }

        public ObjectId? Salvar(Arquivo entidade)
        {
            ObjectId? id;

            try
            {
                var collection = this.Database.ExecuteQuery<Arquivo>(CollectionName);

                if (collection.Find(a => a.Id == entidade.Id).SingleOrDefault() == null)
                {
                    collection.InsertOne(entidade);
                }
                else
                {
                    collection.ReplaceOne(a => a.Id == entidade.Id, entidade);
                }

                id = entidade.Id;
            }
            catch (Exception exception)
            {
                throw new Exception("ArquivoMongoRepositorio > Salvar", exception);
            }

            return id;
        }

        protected override void PersistNewItem(Arquivo item)
        {
            var collection = this.Database.ExecuteQuery<Arquivo>(CollectionName);

            collection.InsertOne(item);
        }

        protected override void PersistUpdatedItem(Arquivo item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistDeletedItem(Arquivo item)
        {
            throw new NotImplementedException();
        }

        #region Métodos de Callback e Auxiliares Privados

        private void AdicionarMensagens(StatusProcessamentoArquivo statusArquivo)
        {
            var builderFilter = Builders<StatusArquivoMessagem>.Filter;

            var filter = builderFilter.Eq("id_arquivo", statusArquivo.IdArquivo) & builderFilter.Eq("tipo", 2);

            var list = this.ExecuteQuery<StatusArquivoMessagem>("arquivos.logs", filter);

            statusArquivo.Mensagem = new List<string>(list.Select(m => m.Mensagem));
        }

        public Arquivo Obter(ObjectId idArquivo)
        {
            try
            {
                var builderFilter = Builders<Arquivo>.Filter;

                var filter = builderFilter.Eq("_id", idArquivo);

                var list = this.ExecuteQuery<Arquivo>(this.CollectionName, filter);

                return list.FirstOrDefault();
            }
            catch (Exception error)
            {
                throw new Exception("ArquivoMongoRepositorio > Obter", error);
            }
        }

        public IList<Arquivo> ObterLista()
        {
            try
            {
                var builderFilter = Builders<Arquivo>.Filter;

                var filter = (builderFilter.Eq("id_status_arquivo", 4) | builderFilter.Eq("id_status_arquivo", 7));

                var list = this.ExecuteQuery<Arquivo>(this.CollectionName, filter);

                return list.ToList();
            }
            catch (Exception error)
            {
                throw new Exception("ArquivoMongoRepositorio > Obter", error);
            }
        }

        // Obtem ultimo arquivo enviado do usuário
        public Arquivo ObterPorUsuario(int idUsuario)
        {
            try
            {
                var builderFilter = Builders<Arquivo>.Filter;

                var filter = builderFilter.Eq("id_usuario", idUsuario);

                var list = this.ExecuteQuery<Arquivo>(this.CollectionName, filter);

                return list.ToList().OrderByDescending(x => x.DataImportacao).FirstOrDefault();
            }
            catch (Exception error)
            {
                throw new Exception("ArquivoMongoRepositorio > ObterPorUsuario", error);
            }
        }

        public Arquivo ObterPorNome(string nome)
        {
            try
            {
                var builderFilter = Builders<Arquivo>.Filter;

                var filter = builderFilter.Eq("nome", nome);

                var list = this.ExecuteQuery<Arquivo>(this.CollectionName, filter);

                return list.FirstOrDefault();
            }
            catch (Exception error)
            {
                throw new Exception("ArquivoMongoRepositorio > ObterPorNome", error);
            }
        }

        #endregion
    }
}
