﻿using maisfidelidade.cnc.bo.api.core.Domain;
using maisfidelidade.cnc.bo.api.core.Infrastructure.Repositorios.MongoDB.Base;
using maisfidelidade.cnc.bo.api.core.Common.Log;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Infrastructure.Repositorios.MongoDB
{
    public class LogMongoRepositorio : MongoRepositorioBase<LogInfo>, ILogMongoRepositorio
    {
        public LogMongoRepositorio()
            : base("arquivos.logs")
        {

        }

        public IList<LogInfo> Obter(ObjectId idArquivo)
        {
            try
            {
                var builderFilter = Builders<LogInfo>.Filter;

                var filter = builderFilter.Eq("id_arquivo", idArquivo);

                var list = this.ExecuteQuery<LogInfo>(this.CollectionName, filter);

                return list;
            }
            catch (Exception error)
            {
                throw new Exception("LogMongoRepositorio > Obter", error);
            }
        }

        public ObjectId? Salvar(LogInfo entidade)
        {
            ObjectId? id;

            try
            {
                var collection = this.Database.ExecuteQuery<LogInfo>(CollectionName);

                if (collection.Find(a => a.Id == entidade.Id).SingleOrDefault() == null)
                {
                    collection.InsertOne(entidade);
                }
                else
                {
                    collection.ReplaceOne(a => a.Id == entidade.Id, entidade);
                }

                id = entidade.Id;
            }
            catch (Exception exception)
            {
                throw new Exception("ArquivoMongoRepositorio > Salvar", exception);
            }

            return id;
        }

        protected override void PersistDeletedItem(LogInfo item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistNewItem(LogInfo item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistUpdatedItem(LogInfo item)
        {
            throw new NotImplementedException();
        }
    }
}
