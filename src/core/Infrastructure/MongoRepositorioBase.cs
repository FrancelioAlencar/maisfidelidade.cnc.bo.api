﻿using maisfidelidade.cnc.bo.api.core.Infrastructure.Data;
using maisfidelidade.cnc.bo.api.core.Infrastructure.Data.MongoDB;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MongoDB.Driver;

namespace maisfidelidade.cnc.bo.api.core.Infrastructure.Repositorios.MongoDB.Base
{
    public abstract class MongoRepositorioBase<TEntityAggregate> : IMongoRepositorio<TEntityAggregate>
    {
        public delegate void AppendChildData(IList<TEntityAggregate> entityAggregate,
            object childEntityKeyValue);

        private readonly MongoDatabase database;
        private string collectionName;

        protected MongoRepositorioBase(string collectionName)
        {
            this.database = MongoDatabaseFactory.CreateDatabase("mongo");
            this.collectionName = collectionName;
        }

        protected MongoDatabase Database
        {
            get => database;
        }

        protected string CollectionName
        {
            get => collectionName;
        }

        protected IList<TEntityAggregate> ExecuteQuery(FilterDefinition<TEntityAggregate> filter)
        {
            var collection = this.database.ExecuteQuery<TEntityAggregate>(collectionName);

            return collection.Find(filter).ToList();
        }

        protected IList<TEntity> ExecuteQuery<TEntity>(string collectionName, FilterDefinition<TEntity> filter)
        {
            var collection = this.database.ExecuteQuery<TEntity>(collectionName);

            return collection.Find(filter).ToList();
        }

        public virtual void PersistNewItem(object item)
        {
            this.PersistNewItem((TEntityAggregate)item);
        }

        public virtual void PersistUpdatedItem(object item)
        {
            this.PersistUpdatedItem((TEntityAggregate)item);
        }

        public virtual void PersistDeletedItem(object item)
        {
            this.PersistDeletedItem((TEntityAggregate)item);
        }

        protected abstract void PersistNewItem(TEntityAggregate item);
        protected abstract void PersistUpdatedItem(TEntityAggregate item);
        protected abstract void PersistDeletedItem(TEntityAggregate item);
    }
}
