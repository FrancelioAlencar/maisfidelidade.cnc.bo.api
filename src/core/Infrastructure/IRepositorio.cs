﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Infrastructure
{
    public interface IRepositorio<TEntityAggregate>
    {
        TEntityAggregate BuscarPor(int id);
        IList<TEntityAggregate> BuscarTodos();
    }
}
