﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Infrastructure
{
    public abstract class RepositorioBase<TEntityAggregate> : IRepositorio<TEntityAggregate>
    {
        #region Membros de IRepositorio
        
        public abstract TEntityAggregate BuscarPor(int id);
        public abstract IList<TEntityAggregate> BuscarTodos(); 
        
        #endregion
    }
}
