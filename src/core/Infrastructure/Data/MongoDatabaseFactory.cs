﻿using maisfidelidade.cnc.bo.api.core.Common.Utility;
using maisfidelidade.cnc.bo.api.core.Infrastructure.Data.MongoDB;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Infrastructure.Data
{
    public static class MongoDatabaseFactory
    {
        private static volatile Func<string, MongoDatabase> createNamedDatabase;

        public static MongoDatabase CreateDatabase(string name)
        {
            return GetCreateDatabase()(name);
        }

        public static void SetDatabases(Func<string, MongoDatabase> createNamedDatabase, bool throwIfSet = true)
        {
            Guard.ArgumentNotNull(createNamedDatabase, nameof(createNamedDatabase));

            Func<string, MongoDatabase> funcCreateNamedDatabase = MongoDatabaseFactory.createNamedDatabase;

            if (funcCreateNamedDatabase != null && throwIfSet)
            {
                throw new InvalidOperationException("Resources.ExceptionDatabaseProviderFactoryAlreadySet");
            }

            MongoDatabaseFactory.createNamedDatabase = createNamedDatabase;
        }

        private static Func<string, MongoDatabase> GetCreateDatabase()
        {
            Func<string, MongoDatabase> createNamedDatabase = MongoDatabaseFactory.createNamedDatabase;

            if (createNamedDatabase == null)
            {
                throw new InvalidOperationException("Resources.ExceptionDatabaseProviderFactoryNotSet");
            }

            return createNamedDatabase;
        }
    }
}
