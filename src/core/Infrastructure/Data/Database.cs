﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Dapper;
using System.Linq;

namespace maisfidelidade.cnc.bo.api.core.Infrastructure.Data
{
    public abstract class Database
    {
        private readonly DbProviderFactory dbProviderFactory;
        private readonly string connectionString;

        protected Database(string connectionString, DbProviderFactory dbProviderFactory)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException("ExceptionNullOrEmptyString", nameof(connectionString));
            }

            if (dbProviderFactory == null)
            {
                throw new ArgumentNullException(nameof(dbProviderFactory));
            }

            this.connectionString = connectionString; //new ConnectionString(connectionString, VALID_USER_ID_TOKENS, VALID_PASSWORD_TOKENS);
            this.dbProviderFactory = dbProviderFactory;
        }

        public string ConnectionString
        {
            get
            {
                return this.connectionString.ToString();
            }
        }

        public virtual DbConnection CreateConnection()
        {
            DbConnection connection = this.dbProviderFactory.CreateConnection();
            connection.ConnectionString = this.ConnectionString;
            return connection;
        }

        internal DbConnection GetNewOpenConnection()
        {
            DbConnection connection = null;
            try
            {
                connection = this.CreateConnection();
                connection.Open();
            }
            catch
            {
                if (connection != null)
                {
                    connection.Close();
                }
                throw;
            }
            return connection;
        }

        protected virtual DatabaseConnectionWrapper GetWrappedConnection()
        {
            return new DatabaseConnectionWrapper(this.GetNewOpenConnection());
        }

        protected DatabaseConnectionWrapper GetOpenConnection()
        {
            return (TransactionScopeConnections.ObterConexao(this) ?? this.GetWrappedConnection());
        }


        //public virtual IList<T> Execute(DbCommand command)
        //{
        //    using (DatabaseConnectionWrapper wrapper = this.GetOpenConnection())
        //    {
        //        var result = command.Connection.Query<T>()

        //        return result;

        //        //PrepareCommand(command, wrapper.Connection);
        //        //IDataReader innerReader = this.DoExecuteReader(command, CommandBehavior.Default);
        //        //return this.CreateWrappedReader(wrapper, innerReader);
        //    }
        //}

        public IList<T> ExecuteQuery<T>(CommandType commandType, string query)
        {
            using (DatabaseConnectionWrapper wrapper = this.GetOpenConnection())
            {
                var result = wrapper.Conexao.Query<T>(query, commandType: commandType).ToList();

                return result;
            }
        }

        public T ExecuteQueryFirst<T> (CommandType commandType, string query)
        {
            using (DatabaseConnectionWrapper wrapper = this.GetOpenConnection())
            {
                var result = wrapper.Conexao.QueryFirst<T>(query, commandType: commandType);

                return result;
            }
        }
    }
}
