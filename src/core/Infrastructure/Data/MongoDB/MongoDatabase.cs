﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Infrastructure.Data.MongoDB
{
    public class MongoDatabase
    {
        private readonly string connectionString;

        public MongoDatabase(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException("ExceptionNullOrEmptyString", nameof(connectionString));
            }

            this.connectionString = connectionString;
        }

        //public virtual IMongoDatabase CreateConnection() // CreateConnection()
        //{
        //    var mongoUrl = MongoUrl.Create(connectionString).DatabaseName;


        //    var connection = new MongoClient(connectionString).GetDatabase(MongoUrl.Create(connectionString).DatabaseName)

            
        //    return null;
        //}

        internal IMongoDatabase GetNewDatabase() // GetNewOpenConnection
        {
            IMongoDatabase database = null;

            try
            {
                database = new MongoClient(connectionString).GetDatabase(MongoUrl.Create(connectionString).DatabaseName);
            }
            catch
            {
                throw;
            }

            return database;
        }


        protected IMongoDatabase GetDatabase() // GetOpenConnection()
        {
            return this.GetNewDatabase();
        }


        public IMongoCollection<T> ExecuteQuery<T>(string collectionName)
        {
            return GetDatabase().GetCollection<T>(collectionName);
        }
    }
}
