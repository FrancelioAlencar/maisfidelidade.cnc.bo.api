﻿using MySql.Data.MySqlClient;

namespace maisfidelidade.cnc.bo.api.core.Infrastructure.Data.MySql
{
    public class MySqlDatabase : Database
    {
        public MySqlDatabase(string connectionString)
            : base(connectionString, MySqlClientFactory.Instance)
        {

        }        
    }
}
