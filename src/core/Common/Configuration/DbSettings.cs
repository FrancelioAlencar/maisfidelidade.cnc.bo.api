﻿using maisfidelidade.cnc.bo.api.core.Common.Log;

namespace maisfidelidade.cnc.bo.api.core.Common.Configuration
{
	public class DbSettings
	{
		private string prefix;

		public string Host { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
		public string Port { get; set; }
		public string Database { get; set; }
		public string Collection { get; set; }
		public string Protocol { get; set; }
		public string Options { get; set; }

		public DbSettings(string prefix, bool passwordBase64 = false, bool protocolBase64 = false)
		{
			this.prefix = prefix;

			Host = ConfigurationManager.AppSettings[this.prefix + "_host"];
			Port = ConfigurationManager.AppSettings[this.prefix + "_port"];
			Username = ConfigurationManager.AppSettings[this.prefix + "_username"];
			Password = ConfigurationManager.AppSettings.Decrypt(this.prefix + "_password", passwordBase64);
			Database = ConfigurationManager.AppSettings[this.prefix + "_database"];
			Collection = ConfigurationManager.AppSettings[this.prefix + "_collection"];
			Protocol = ConfigurationManager.AppSettings[this.prefix + "_protocol"];
			Options = ConfigurationManager.AppSettings[this.prefix + "_options"];
			if (protocolBase64) Protocol = Protocol.DecodeBase64();
			Logger.Log($"prefix: {prefix}, host: {Host}");
		}

		public string ConnectionString()
		{
			var connectionString = string.Empty;

			if (prefix.ToLower().Contains("_mysql") || prefix.ToLower().Contains("_sqlserver"))
				connectionString = $"Server={Host};Database={Database};Uid={Username};Pwd={Password};Convert Zero Datetime=True;Allow Zero Datetime=true;";

			if (prefix.ToLower().Contains("_mongodb"))
				connectionString = $"{Protocol}://{Username}:{Password}@{Host}/{Database}?{Options}";
			
			return connectionString;
		}
	}
}
