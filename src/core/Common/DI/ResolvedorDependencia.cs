using Microsoft.Extensions.DependencyInjection;
using System;

namespace maisfidelidade.cnc.bo.api.core.Common.DI
{
    public class ResolvedorDependencia
    {
        private readonly IServiceProvider provedorServico;

        public ResolvedorDependencia()
        {
            var serviceCollection = new ServiceCollection();

            AdicionarVinculos(serviceCollection);

            provedorServico = serviceCollection.BuildServiceProvider();
        }

        public T ObterServico<T>()
        {
            try
            {
                return provedorServico.GetService<T>();
            }
            catch  (Exception exception)
            {
                throw new Exception("", exception);
            }
        }

        public object ObterServico(Type servicoType)
        {
            try
            {
                return provedorServico.GetService(servicoType);
            }
            catch (Exception exception)
            {
                throw new Exception("", exception);
            }
        }
        
        private void AdicionarVinculos(IServiceCollection servicos)
        {
            //servicos.AddTransient<ICategoriaRepositorio, CategoriaRepositorio>();
            //servicos.AddTransient<ICriterioRepositorio, CriterioRepositorio>();
            //servicos.AddTransient<IResgateRepositorio, ResgateRepositorio>();
            //servicos.AddTransient<IArquivoRepositorio, ArquivoRepositorio>();
            //servicos.AddTransient<IArquivoMongoRepositorio, ArquivoMongoRepositorio>();
            //servicos.AddTransient<ILogMongoRepositorio, LogMongoRepositorio>();
        }
    }
}
