﻿//using maisfidelidade.veiculos.importacao.core.Domain.Model.Enums;
using maisfidelidade.cnc.bo.api.core.Domain.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.bo.api.core.Common.Log
{
    public class LogInfo
    {
        public LogInfo()
        {
            Detalhes = new List<LogDetalhe>();
        }

        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("id_arquivo")]
        public ObjectId? IdArquivo { get; set; }

        [BsonElement("id_status_processamento")]
        public StatusAndamentoProcessoEnum StatusAndamentoProcesso { get; set; }

        [BsonElement("id_status_log")]
        public StatusLogEnum StatusLog { get; set; }

        [BsonElement("data_inicio")]
        public DateTime? DataInicio { get; set; }

        [BsonElement("data_fim")]
        public DateTime? DataFim { get; set; }

        [BsonElement("tempo_decorrido")]
        public TimeSpan TempoDecorrido { get; set; }

        [BsonElement("tipo")]
        public TipoLog Tipo { get; set; }

        [BsonElement("mensagem")]
        public string Mensagem { get; set; }

        [BsonElement("stack_trace")]
        public string StackTrace { get; set; }

        [BsonElement("detalhes")]
        public IList<LogDetalhe> Detalhes { get; set; }
    }
}
