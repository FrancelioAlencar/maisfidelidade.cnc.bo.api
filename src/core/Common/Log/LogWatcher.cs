﻿using System;
using System.Diagnostics;

namespace maisfidelidade.cnc.bo.api.core.Common.Log
{
    public class LogWatcher
    {
        private readonly Stopwatch stopwatch;

        public LogWatcher()
        {
            stopwatch = new Stopwatch();
        }

        public DateTime? Inicio { get; private set; }
        public DateTime? Termino { get; private set; }
        public TimeSpan TempoDecorrido
        {
            get { return stopwatch.Elapsed; }
        }

        public void Iniciar()
        {
            if (!stopwatch.IsRunning)
            {
                Inicio = DateTime.Now;

                stopwatch.Restart();
                stopwatch.Start();
            }
        }

        public void Parar()
        {
            if (stopwatch.IsRunning)
            {
                Termino = DateTime.Now;

                stopwatch.Stop();
            }
        }
    }
}
